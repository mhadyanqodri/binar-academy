import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { Truck, Camera, Key, Box } from "../../assets";
import { WARNA_ICON } from '../../utils/constant';

const ButtonIcon = ({ title }) => {
    const Icon = () => {
        if (title == "Sewa Mobil") return <Truck/>

        if (title == "Oleh-Oleh") return <Box/>

        if (title == "Penginapan") return <Key/>

        if (title == "Wisata") return <Camera/>

        return <Truck />
    }

    return (
        <TouchableOpacity>
            <View style={styles.button}>
                <Icon/>
            </View>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

export default ButtonIcon

const styles = StyleSheet.create({
    button: {
        backgroundColor: WARNA_ICON,
        borderRadius: 8,
        padding: 22
    },
    text: {
        fontSize: 12,
        textAlign: 'center'
    }
})