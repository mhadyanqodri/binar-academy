import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions } from 'react-native'
import React from 'react'
import { IconTas, IconUsers, Mobil } from "../../assets";

const ListMobil = () => {
    return (
        <TouchableOpacity style={styles.container}>
            <Image source={Mobil} style={styles.mobil}></Image>
            <View style={styles.namalistMobil}>
                <Text>Daihatsu Xenia</Text>
                <View style={styles.iconlistMobil}>
                    <View style={styles.iconUsers}>
                        <IconUsers />
                        <Text>4</Text>
                    </View>
                    <View style={styles.iconTas}>
                        <IconTas />
                        <Text>4</Text>
                    </View>
                </View>
                <Text style={styles.harga}>Rp 230.000</Text>
            </View>
        </TouchableOpacity>
    )
}

export default ListMobil

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: 'white',
        padding: 17,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
        marginVertical: windowHeight * 0.013
    },
    iconlistMobil: {
        flexDirection: 'row',
        paddingTop: 5
    },
    namalistMobil: {
        marginLeft: 20
    },
    iconUsers: {
        flexDirection: 'row'
    },
    iconTas: {
        flexDirection: 'row',
        paddingLeft: 15
    },
    harga: {
        color: '#5CB85F'
    }
})