import { StyleSheet, Text, View, ImageBackground, Dimensions, Image } from 'react-native'
import React from 'react'
import { ScrollView } from 'react-native-gesture-handler';
import Font from '../../assets/fonts/Font';
import { ButtonIcon, ListMobil } from '../../components';
import { WARNA_BODY } from '../../utils/constant';

const DaftarMobil = () => {
  return (
    <View style={styles.page}>
      <ScrollView showsHorizontalScrollIndicator={false}>
        <View style={styles.listMobil}>
          <Text style={styles.navbar}>Daftar Mobil</Text>
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
        </View>
      </ScrollView>
    </View>
  )
}

export default DaftarMobil

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: WARNA_BODY
  },
  listMobil: {
    paddingHorizontal: 30,
    paddingTop: 25,
  },
  navbar: {
    fontSize: 18,
    marginBottom: 10,
    color: 'black',
    fontFamily: Font.PoppinsBold
  }
})