import IconHome from './Home.svg'
import IconHomeActive from './HomeActive.svg'
import IconDaftarMobil from './DaftarMobil.svg'
import IconDaftarMobilActive from './DaftarMobilActive.svg'
import IconAkun from './Akun.svg'
import IconAkunActive from './AkunActive.svg'

import Box from "./Box.svg";
import Camera from "./Camera.svg";
import Key from "./Key.svg";
import Truck from "./Truck.svg";

import IconUsers from "./IconUsers.svg";
import IconTas from "./IconTas.svg";
import IconMobil from "./IconMobil.svg";

import Animasi from "./Animasi.svg";

export { 
    IconHome,
    IconHomeActive, 
    IconDaftarMobil, 
    IconDaftarMobilActive, 
    IconAkun, 
    IconAkunActive, 
    Box, 
    Camera, 
    Key, 
    Truck,
    IconUsers,
    IconTas,
    IconMobil,
    Animasi
}