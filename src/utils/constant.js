export const WARNA_UTAMA = '#0D28A6';
export const WARNA_DISABLE = '#C8C8C8';
export const WARNA_ICON = '#DEF1DF';
export const WARNA_BODY = '#FFFFFF';